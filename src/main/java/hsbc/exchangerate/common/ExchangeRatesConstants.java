package hsbc.exchangerate.common;

public class ExchangeRatesConstants {

	public static final String EXCHANGE_SYMBOLS = "GBP,USD,HKD";
	public static final String EXCHANGE_ACCESS_KEY = "3647492400e2178af30b24d5919d0b98";
	public static final String EXCHANGE_REST_URL_DATE_RANGE = "http://api.exchangeratesapi.io/v1/timeseries?access_key="
			+ EXCHANGE_ACCESS_KEY + "&start_date={start_date}&end_date={end_date}";
	public static final String EXCHANGE_REST_URL_LATEST = "http://api.exchangeratesapi.io/v1/latest?access_key="
			+ EXCHANGE_ACCESS_KEY + "&symbols=" + EXCHANGE_SYMBOLS;
	public static final String EXCHANGE_REST_URL_DATE = "http://api.exchangeratesapi.io/v1/{date}?access_key="
			+ EXCHANGE_ACCESS_KEY + "&symbols=" + EXCHANGE_SYMBOLS;

}
