package hsbc.exchangerate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hsbc.exchangerate.pojo.ExchangeRateResponse;
import hsbc.exchangerate.service.impl.RatesAPIServiceImpl;

@RestController
@EnableAutoConfiguration
@RequestMapping("/exchangeRate")
public class ExchangeRateController {

	@Autowired
	RatesAPIServiceImpl ratesAPIService;

	@RequestMapping("/loadData/{date}")
	public ExchangeRateResponse loadData(@PathVariable String date) {
		return ratesAPIService.loadData(date);
	}

	@RequestMapping("/readData")
	public List<Object> readData(@RequestParam(value = "date", required = false) String date,
			@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate,
			@RequestParam(value = "currency", required = false) String exchangeCurrency) {
		return ratesAPIService.readData(date, fromDate, toDate, exchangeCurrency);
	}
}
