package hsbc.exchangerate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ExchangeRates")
public class ExchangeRates {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id", nullable = false)
	private Long id;

	@Column(name = "BaseCurrency", nullable = true)
	private String baseCurrency;

	@Column(name = "ExchangeCurrency", nullable = true)
	private String exchangeCurrency;

	@Column(name = "Rate", nullable = true)
	private double rate;

	@Column(name = "Date", nullable = true)
	private String date;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getExchangeCurrency() {
		return exchangeCurrency;
	}

	public void setExchangeCurrency(String exchangeCurrency) {
		this.exchangeCurrency = exchangeCurrency;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ExchangeRates [id=" + id + ", baseCurrency=" + baseCurrency + ", exchangeCurrency=" + exchangeCurrency
				+ ", rate=" + rate + ", date=" + date + "]";
	}

}
