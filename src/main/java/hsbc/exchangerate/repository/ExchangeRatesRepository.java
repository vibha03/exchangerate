package hsbc.exchangerate.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hsbc.exchangerate.entity.ExchangeRates;

@Repository
public interface ExchangeRatesRepository extends CrudRepository<ExchangeRates, Long> {

	@Query("SELECT ex FROM ExchangeRates ex WHERE ex.exchangeCurrency IN ?3 AND ex.date BETWEEN ?1 AND ?2")
	public List<Object> searchByDateRangeAndCurrency(String fromDate, String toDate,
			Collection<String> exchangeCurrency);

	@Query("SELECT ex FROM ExchangeRates ex WHERE ex.exchangeCurrency IN ?1 ")
	public List<Object> searchByCurrency(Collection<String> exchangeCurrency);

	@Query("SELECT ex FROM ExchangeRates ex WHERE ex.date = ?1 ")
	public List<Object> searchByDate(String date);

	@Query("SELECT ex FROM ExchangeRates ex WHERE ex.exchangeCurrency IN ?2 AND ex.date = ?1 ")
	public List<Object> searchByDateAndCurrency(String date, Collection<String> exchangeCurrency);

	@Query("SELECT ex FROM ExchangeRates ex WHERE ex.date BETWEEN ?1 AND ?2")
	public List<Object> searchByDateRange(String fromDate, String toDate);

}
