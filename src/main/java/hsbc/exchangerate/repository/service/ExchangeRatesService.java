package hsbc.exchangerate.repository.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hsbc.exchangerate.entity.ExchangeRates;
import hsbc.exchangerate.repository.ExchangeRatesRepository;

@Service
public class ExchangeRatesService {

	Logger logger = LoggerFactory.getLogger(ExchangeRatesService.class);

	@Autowired
	private ExchangeRatesRepository exchangeRatesRepository;

	public Iterable<ExchangeRates> findAll() {
		return exchangeRatesRepository.findAll();
	}

	public boolean saveAll(Iterable<ExchangeRates> exchangeRates) {
		exchangeRatesRepository.saveAll(exchangeRates);
		return true;
	}

	public List<Object> searchData(String date, String fromDate, String toDate, List<String> exchangeCcy) {
		try {
			if (date != null && exchangeCcy != null && fromDate == null && toDate == null) {
				return exchangeRatesRepository.searchByDateAndCurrency(date, exchangeCcy);
			} else if (date == null && exchangeCcy == null && fromDate != null && toDate != null) {
				return exchangeRatesRepository.searchByDateRange(fromDate, toDate);
			} else if (date != null && exchangeCcy == null && fromDate == null && toDate == null) {
				return exchangeRatesRepository.searchByDate(date);
			} else if (date == null && exchangeCcy != null && fromDate == null && toDate == null) {
				return exchangeRatesRepository.searchByCurrency(exchangeCcy);
			}
			return exchangeRatesRepository.searchByDateRangeAndCurrency(fromDate, toDate, exchangeCcy);
		} catch (Exception e) {
			logger.error("ExchangeRatesService | searchData | Exception occured : ", e);
			return new ArrayList<>();
		}
	}
}
