package hsbc.exchangerate.service;

import java.util.List;

import hsbc.exchangerate.pojo.ExchangeRateResponse;

public interface RatesAPIService {

	public ExchangeRateResponse loadData(String date);

	List<Object> readData(String date, String fromDate, String toDate, String exchangeCurrency);

}
