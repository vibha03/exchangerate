package hsbc.exchangerate.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import hsbc.exchangerate.common.ExchangeRatesConstants;
import hsbc.exchangerate.entity.ExchangeRates;
import hsbc.exchangerate.pojo.ExchangeRateResponse;
import hsbc.exchangerate.repository.service.ExchangeRatesService;
import hsbc.exchangerate.service.RatesAPIService;

@Service
public class RatesAPIServiceImpl implements RatesAPIService {

	Logger logger = LoggerFactory.getLogger(RatesAPIServiceImpl.class);

	@Autowired
	ExchangeRatesService exchangeRatesService;

	@Override
	public ExchangeRateResponse loadData(String date) {
		Gson gson = new Gson();
		ExchangeRateResponse exchangeRateResponse = null;
		if (!validateDateFormat(date)) {
			exchangeRateResponse = new ExchangeRateResponse(false,
					"Invalid date. Please enter date in [YYYY-MM-DD] format ");
			logger.error("Date format is not correct");
			return exchangeRateResponse;
		}
		try {
			exchangeRateResponse = gson.fromJson(invokeExchangeRateAPI(date), ExchangeRateResponse.class);

			List<ExchangeRates> exchangeRates = new ArrayList<>();

			for (String currencyCode : exchangeRateResponse.getRates().keySet()) {
				ExchangeRates exchangeRate = new ExchangeRates();
				exchangeRate.setBaseCurrency(exchangeRateResponse.getBase());
				exchangeRate.setDate(exchangeRateResponse.getDate());
				exchangeRate.setExchangeCurrency(currencyCode);
				exchangeRate.setRate(exchangeRateResponse.getRates().get(currencyCode));
				exchangeRates.add(exchangeRate);
			}
			exchangeRatesService.saveAll(exchangeRates);
		} catch (Exception e) {
			exchangeRateResponse = new ExchangeRateResponse(false, "Error Occured");
			logger.error("Exception occured while loading data into database: ", e);
		}
		return exchangeRateResponse;
	}

	private boolean validateDateFormat(String date) {
		if (date != null)
			return date.matches("^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$");
		return true;
	}

	@Override
	public List<Object> readData(String date, String fromDate, String toDate, String exchangeCurrency) {

		List<Object> dateFormats = new ArrayList<>();
		if (!validateDateFormat(date)) {
			dateFormats.add("Invalid format of date param, Please enter date in [YYYY-MM-DD] format.");
		}
		if (!validateDateFormat(fromDate)) {
			dateFormats.add("Invalid format of fromDate param, Please enter date in [YYYY-MM-DD] format.");
		}
		if (!validateDateFormat(toDate)) {
			dateFormats.add("Invalid format of toDate param, Please enter date in [YYYY-MM-DD] format.");
		}

		if (!dateFormats.isEmpty()) {
			logger.error("Date format is not correct");
			return dateFormats;
		}

		List<String> exchangeCcyList = exchangeCurrency != null ? Arrays.asList(exchangeCurrency.split(",")) : null;
		return exchangeRatesService.searchData(date, fromDate, toDate, exchangeCcyList);
	}

	private String invokeExchangeRateAPI(String date) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(ExchangeRatesConstants.EXCHANGE_REST_URL_DATE, String.class, date);
	}

}
