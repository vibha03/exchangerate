package hsbc.exchangerate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import hsbc.exchangerate.entity.ExchangeRates;
import hsbc.exchangerate.pojo.ExchangeRateResponse;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RatesAPIServiceImplTest {

	private final static String DATE = "2012-05-25";
	private final static String CURRENCY = "GBP";
	private final static String INVALID_DATE = "2012-15-25";

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	@Order(1)
	public void loadDataShouldReturnDataForValidDate() throws Exception {
		ExchangeRateResponse response = this.restTemplate.getForObject(
				"http://localhost:" + port + "/exchangeRate/loadData/{date}", ExchangeRateResponse.class, DATE);
		assertThat(response.getDate().equals(DATE));
		assertTrue(response.isSuccess());
	}

	@Test
	@Order(2)
	public void loadDataShouldReturnErrorOfInvalidDate() throws Exception {
		ExchangeRateResponse response = this.restTemplate.getForObject(
				"http://localhost:" + port + "/exchangeRate/loadData/{date}", ExchangeRateResponse.class, INVALID_DATE);
		assertFalse(response.isSuccess());
		assertThat(response.getErrorMessage()).contains("Invalid date");
	}

	@Test
	@Order(3)
	public void readDataShouldReturnDataForDateRange() throws Exception {
		Map<String, String> dateMap = new HashMap<>();
		dateMap.put("fromDate", "2012-05-21");
		dateMap.put("toDate", "2012-05-25");
		ObjectMapper mapper = new ObjectMapper();
		JsonNode response = this.restTemplate.getForObject(
				"http://localhost:" + port + "/exchangeRate/readData?fromDate={fromDate}&toDate={toDate}",
				JsonNode.class, dateMap);
		List<ExchangeRates> exchangeRates = mapper.convertValue(response, new TypeReference<List<ExchangeRates>>() {
		});
		assertFalse(exchangeRates.isEmpty());
		assertThat(exchangeRates.get(0).getBaseCurrency()).isEqualTo("EUR");
	}

	@Test
	@Order(4)
	public void readDataShouldReturnErrorOfInvalidDate() throws Exception {
		Map<String, String> dateMap = new HashMap<>();
		dateMap.put("fromDate", INVALID_DATE);
		dateMap.put("toDate", INVALID_DATE);
		List<String> response = this.restTemplate.getForObject(
				"http://localhost:" + port + "/exchangeRate/readData?fromDate={fromDate}&toDate={toDate}", List.class,
				dateMap);
		assertTrue(response.stream().allMatch(msg -> msg.contains("Invalid format")));
	}

	@Test
	@Order(5)
	public void readDataShouldReturnDataForSingleDate() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode response = this.restTemplate
				.getForObject("http://localhost:" + port + "/exchangeRate/readData?date={date}", JsonNode.class, DATE);
		List<ExchangeRates> exchangeRates = mapper.convertValue(response, new TypeReference<List<ExchangeRates>>() {
		});
		assertFalse(exchangeRates.isEmpty());
		assertThat(exchangeRates.get(0).getDate()).isEqualTo(DATE);
	}

	@Test
	@Order(6)
	public void readDataShouldReturnDataForSingleDateAndCurrency() throws Exception {
		Map<String, String> testDataMap = new HashMap<>();
		testDataMap.put("date", DATE);
		testDataMap.put("currency", CURRENCY);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode response = this.restTemplate.getForObject(
				"http://localhost:" + port + "/exchangeRate/readData?date={date}&currency={currency}", JsonNode.class,
				testDataMap);
		List<ExchangeRates> exchangeRates = mapper.convertValue(response, new TypeReference<List<ExchangeRates>>() {
		});
		assertFalse(exchangeRates.isEmpty());
		assertThat(exchangeRates.get(0).getExchangeCurrency()).isEqualTo(CURRENCY);
	}
}
